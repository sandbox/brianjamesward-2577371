(function ($) {

	Drupal.behaviors.timeTable = {
		attach: function () {

			var rows = Drupal.settings.viewsTimetable.rows,
					options = Drupal.settings.viewsTimetable.display_options,
					curDateStart = new Date(new Date().setHours(0,0,0,0)),
					curDateEnd = new Date(new Date().setHours(24,0,0,0)),
					curDateScale = new Date(new Date().setHours(04,0,0,0));

			glow.ready(function() {

				var viewsTimetable = new glow.widgets.Timetable("#timetable",
						curDateStart, curDateEnd,
						curDateStart, curDateScale,
						{
							vertical: (options.timetable_vertical ? options.timetable_vertical : 0),
							size: (options.timetable_size ? options.timetable_size : 1040),
							theme: (options.timetable_theme ? options.timetable_theme : 'dark'),
							trackHeader: "<div class='track'><h4>{title}</h4></div>",
							collapseTrackBorders: true,
						}
				);

				// Set the scale of the timetable.
				viewsTimetable.addScale("hour", "left", 40, {
					template: function(data){

						var date = new Date(data.start);
						var time = new Intl.DateTimeFormat("en-US", {hour: "numeric", minute: "numeric"}).format(date);

						return time;
					}
				});

				viewsTimetable.setItemTemplate("<h4>{data.title}</h4><span class='date'>{data.date}</span>");

				// Add tracks and track items.
				for(var i = 0; i < rows.length; i++) {

					var trackName = rows[i].track,
							startPoint = rows[i].startPoint,
							endPoint = rows[i].endPoint;
							title = rows[i].title;

					// Add the track to the timetable.
					var newTrack = viewsTimetable.addTrack(trackName, 100);

					// Get hours and minutes of start and end time.
					var itemStartTime = new Intl.DateTimeFormat("en-US", {hour: "numeric", minute: "numeric"}).format(new Date(startPoint)),
							itemEndTime = new Intl.DateTimeFormat("en-US", {hour: "numeric", minute: "numeric"}).format(new Date(endPoint));

					// Add the item to the track with body if it exists.
					newTrack.addItem(trackName, startPoint, endPoint, {
						data: {
							title: title,
							date: itemStartTime + ' - ' + itemEndTime
						}
					});

				}

				// If the "scrollbar" option has been selected.
				if(options.timetable_scrollbar) {
					viewsTimetable.addScrollbar(
							"hour", "top", 50,
							{
								template: function(data){
									return data.start.getHours()
								}
							}
					);
				}

				// Draw the timetable.
				viewsTimetable.draw();

				// Set the position to the current date and time.
				viewsTimetable.currentPosition(new Date());

			});


		}
	};

})(jQuery);
