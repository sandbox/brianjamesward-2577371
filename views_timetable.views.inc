<?php

/**
 * Define the views_timetable style plugin.
 *
 * Implements hook_views_plugins().
 */
function views_timetable_views_plugins() {

  $plugins = array(
    'style' => array(
      'timetable' => array(
        'title' => t('Timetable'),
        'help' => t('Displays rows in a timetable.'),
        'handler' => 'views_plugin_style_timetable',
        'theme' => 'views_view_timetable',
        'uses fields' => TRUE,
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
    ),
  );

  return $plugins;
}
