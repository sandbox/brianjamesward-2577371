<?php

/**
 * @file
 * Contains the timetable style plugin.
 */

/**
 * Style plugin to render each item as a timetable cell.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_timetable extends views_plugin_style {

  /**
   * Set default options.
   *
   * @return array
   */
  function option_definition() {
    $options = parent::option_definition();

    // Main configuration options.
    $options['config']['track_field'] = '';
    $options['config']['date_field'] = '';
    $options['config']['title_field'] = '';

    // Timetable display settings.
    $options['display']['timetable_size'] = '';
    $options['display']['timetable_vertical'] = '';
    $options['display']['timetable_theme'] = '';
    $options['display']['timetable_scrollbar'] = '';

    return $options;
  }

  /**
   * Render the style plugin configuration form.
   *
   * @param $form
   * @param $form_state
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    if ($this->uses_fields()) {
      $options = array('' => t('- None -'));
      $field_labels = $this->display->handler->get_field_labels(TRUE);
      $options += $field_labels;
    }

    $form['config'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
    );

    $form['config']['track_field'] = array(
      '#type' => 'select',
      '#title' => t('Select track field'),
      '#description' => t('The track field is used to group rows together.'),
      '#default_value' => $this->options['config']['track_field'],
      '#required' => FALSE,
      '#options' => $options,
    );

    $form['config']['date_field'] = array(
      '#type' => 'select',
      '#title' => t('Select date field'),
      '#description' => t('This must be a valid Date (ISO format) field, with end date. This field is used to plot the item on the timetable.'),
      '#default_value' => $this->options['config']['date_field'],
      '#required' => FALSE,
      '#options' => $options,
    );

    $form['config']['title_field'] = array(
      '#type' => 'select',
      '#title' => t('Select title field'),
      '#description' => t(
        'This field will be rendered, therefor any style or links you add will also be rendered with this field.'
      ),
      '#default_value' => $this->options['config']['title_field'],
      '#required' => FALSE,
      '#options' => $options,
    );

    $form['display'] = array(
      '#type' => 'fieldset',
      '#title' => t('Display settings'),
    );

    $form['display']['timetable_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Timetable size'),
      '#description' => t('The width (if horizontal) or height (if vertical) of the scrollable area of the Timetable.'),
      '#default_value' => $this->options['display']['timetable_size'],
      '#required' => FALSE,
    );

    $form['display']['timetable_vertical'] = array(
      '#type' => 'checkbox',
      '#title' => t('Timetable vertical'),
      '#description' => t('Create a vertical Timetable?'),
      '#default_value' => $this->options['display']['timetable_vertical'],
      '#required' => FALSE,
    );

    $form['display']['timetable_theme'] = array(
      '#type' => 'select',
      '#title' => t('Timetable theme'),
      '#description' => t('Possible values are "light" and "dark". Both themes can be altered with CSS to fit the design of your site.'),
      '#default_value' => $this->options['display']['timetable_theme'],
      '#required' => FALSE,
      '#options' => array(
        'dark' => 'dark',
        'light' => 'light'
      ),
    );

    $form['display']['timetable_scrollbar'] = array(
      '#type' => 'checkbox',
      '#title' => t('Timetable scrollbar'),
      '#description' => t('Adds a scrollbar to one or both sides of the Timetable.'),
      '#default_value' => $this->options['display']['timetable_scrollbar'],
      '#required' => FALSE,
    );

  }

  /**
   * Pass the views data to the timetable javascript file
   * for processing.
   *
   * Implementation of view_style_plugin::render()
   *
   * @return string
   * @throws \Exception
   */
  function render() {

    // Don't bother doing anything unless the Glow library has been loaded.
    if (($library = libraries_load('glow')) && !empty($library['loaded'])) {

      $this->addAssets();

      $view = $this->view;
      $options = $this->options;
      $config_options = $this->options['config'];
      $display_options = $this->options['display'];

      $rows = array();
      foreach ($view->result as $index => $row) {
        $view->row_index = $index;

        // Get the track field value for this row item.
        $track_field = $this->get_field($index, $config_options['track_field']);

        // Get the track date (ISO format) values for this row item.
        $date_field = $this->get_field($index, $config_options['date_field']);

        // Get the track title value for this row item.
        $title_field = $this->get_field($index, $config_options['title_field']);

        // If the item has the required fields, add the item to results.
        if(!empty($track_field) && !empty($date_field) && !empty($title_field)) {

          // Get the values for the date field.
          $date_field = $this->get_field_value($index, $config_options['date_field']);

          $rows[] = array(
            'track' => $track_field,
            'startPoint' => $date_field[0]['value'],
            'endPoint' => $date_field[0]['value2'],
            'title' => $title_field,
          );

          // Pass the view's data to the javascript.
          drupal_add_js(
            array(
              'viewsTimetable' => array(
                'display_options' => $display_options,
                'rows' => $rows,
              ),
            ),
            'setting'
          );

        } else {
          drupal_set_message('Timetable could not be rendered. This could either be due to an error or that there is no content for the timetable to render.', 'warning');
        }
      }

      unset($view->row_index);

      return theme(
        'views_view_timetable',
        array(
          'view' => $view,
          'options' => $options,
          'rows' => $rows,
        )
      );

    }

  }

  /**
   * Add the timetable assets.
   */
  function addAssets() {
    drupal_add_js(drupal_get_path('module', 'views_timetable') . '/js/timetable.js');
    drupal_add_css(drupal_get_path('module', 'views_timetable') . '/css/timetable.css');
  }

}
